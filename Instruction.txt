1. git clone 
2. upzip config files from 3 folders (DotNetCoders, DotNetCoders.DatabaseContext, DotNetCoders.Repository)
3. No need to create database/migration
	a. Just restore backup file or generate script file which is attached.
4. No need to create model in dotnetcoders.model
5. No need to create DbSet in dotnetcoders.DatabaseContext
6. Only work on UI like Model(For ViewModel), View, Controller
7. You can follow my coding if necessary
8. No need to push file to gitlab.com
**For your kind information 
I have already installed EntityFramework, Unobtrusive.Validation, Mapper and added all referrence for coding. 
So you need not to install those.
Happy Coding & Sorry for my late.