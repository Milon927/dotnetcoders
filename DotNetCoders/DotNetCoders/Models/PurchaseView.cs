﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DotNetCoders.Model.Model;

namespace DotNetCoders.Models
{
    public class PurchaseView
    {
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public string InvoiceNo { get; set; }
        public DateTime ManufacturedDate { get; set; }
        public DateTime ExpiredDate { get; set; }
        public int Quantity { get; set; }
        public string Remarks { get; set; }
        public double UnitPrice { get; set; }
        public double TotalPrice { get; set; }
        public double MRP { get; set; }
        public string Code { get; set; }
        public int SupplierId { get; set; }
        public int CategoryId { get; set; }
        public int ProductId { get; set; }
        public List<PurchaseInfo> PurchaseInfos { get; set; }
        public List<PurchaseProductInfo> PurchaseProductInfos { get; set; }
    }
}