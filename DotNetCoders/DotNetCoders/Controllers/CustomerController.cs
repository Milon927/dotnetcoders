﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using DotNetCoders.Manager.Manager;
using DotNetCoders.Model.Model;
using DotNetCoders.Models;

namespace DotNetCoders.Controllers
{
    public class CustomerController : Controller
    {
        CustomerManager _customerManager = new CustomerManager();
        // GET: Customer
        [HttpGet]
        public ActionResult Add()
        {
            CustomerView _customerView = new CustomerView();
            ViewBag.Message = null;
            ViewBag.ActionName = "Add";
            ViewBag.ButtonName = "Register";
            return View(_customerView);
        }
        [HttpPost]
        public ActionResult Add(CustomerView customerView)
        {
            //CustomerView _customerView = new CustomerView();
            string message = null;
            ViewBag.ActionName = "Add";
            ViewBag.ButtonName = "Register";
            Customer customer = Mapper.Map<Customer>(customerView);
            if (_customerManager.Add(customer))
            {
                message = "saved";
            }
            else
            {
                message = "Not saved";
            }
            ViewBag.Message = message;
            return View(customerView);
        }
        [HttpGet]
        public ActionResult Show()
        {
            CustomerView customerView = new CustomerView();
            customerView.Customers = _customerManager.GetAll();
            return View(customerView);
        }
        [HttpGet]
        public ActionResult Edit(int id)
        {
            CustomerView customerView = new CustomerView();
            Customer customer = _customerManager.GetById(id);
            customerView = Mapper.Map<CustomerView>(customer);
            ViewBag.ActionName = "Edit";
            ViewBag.ButtonName = "Update";
            ViewBag.Message = null;
            return View(customerView);
        }
        [HttpPost]
        public ActionResult Edit(CustomerView customerView)
        {
            //CustomerView customerView = new CustomerView();
            Customer customer = Mapper.Map<Customer>(customerView);
            //customerView = Mapper.Map<CustomerView>(customer);
            if (_customerManager.Update(customer))
            {
                ViewBag.Message = "Update";
            }
            ViewBag.ActionName = "Edit";
            ViewBag.ButtonName = "Update";
            return View(customerView);
        }
        [HttpPost]
        public ActionResult Delete(int id)
        {
            if (_customerManager.Delete(id))
            {
                ViewBag.Message = "Deleted";
            }
            else
            {
                ViewBag.Message = "Not deleted";

            }
            return RedirectToAction("Show");
        }

        [HttpGet]
        public ActionResult Search()
        {
            CustomerView customerView = new CustomerView();
            customerView.Customers = _customerManager.GetAll();
            return View(customerView);
        }

        [HttpPost]
        public ActionResult Search(string search)
        {
            CustomerView customerView = new CustomerView();
           customerView.Customers =  _customerManager.SearchCustomers(search);
           return View(customerView);
        }
    }
}