﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.AccessControl;
using System.Text;
using System.Threading.Tasks;

namespace DotNetCoders.Model.Model
{
    public class PurchaseProductInfo
    {
        public int Id { get; set; }
        public int Quantity { get; set; }
        public double UnitPrice { get; set; }
        public double MRP { get; set; }
        public DateTime ManufacturedDate { get; set; }
        public DateTime ExpiredDate { get; set; }
        //public string Code { get; set; }
        public string Remarks { get; set; }
        public int PurchaseInfoId { get; set; }
        public int ProductId { get; set; }
        public Product Product { get; set; }
        public PurchaseInfo PurchaseInfo { get; set; }
    }
}
